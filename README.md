# cloudflare-ddns

A dynamic DNS script written in python that uses CloudFlare's free DNS and their API to set up a dynamic DNS record pointing to your server.

## How To Use

```
git clone https://gitlab.com/aamjadi/cloudflare-ddns.git
cd cloudflare-ddns
### Edit ddns.py - enter your CloudFlare credentials and domain details
```


if everything works, put it in your crontab.

```
@reboot /path/to/cloudflare-ddns/ddns.py
```


